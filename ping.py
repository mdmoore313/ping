from socket import *
import os 
import sys 
import struct 
import time 
import select 
import binascii


#Constants
ICMP_TYPE_ECHO_REPLY = 0
ICMP_TYPE_REQ_TIMEOUT = 1
ICMP_TYPE_DEST_UNREACH = 3
ICMP_TYPE_ECHO_REQ = 8

ICMP_CODE_NET_UNREACH = 0
ICMP_CODE_HOST_UNREACH = 1
ICMP_CODE_PROTO_UNREACH = 2
ICMP_CODE_PORT_UNREACH = 3
ICMP_CODE_DESTNET_UNKNO = 6
ICMP_CODE_DESTHOST_UNKNO = 7


def checksum(str):
	csum = 0
	countTo = (len(str) / 2) * 2

	count = 0
	while count < countTo:
		thisVal = ord(str[count+1]) * 256 + ord(str[count])
		csum = csum + thisVal
		csum = csum & 0xffffffffL
		count = count + 2

	if countTo < len(str):
		csum = csum + ord(str[len(str) -1])
		csum = csum & 0xffffffffL

	csum = (csum >> 16) + (csum & 0xffff)
	csum = csum + (csum >> 16)

	answer = ~csum
	answer = answer & 0xffff
	answer = answer >> 8 | (answer << 8 & 0xff00)

	return answer
#End checksum

def receiveOnePing(mySocket, ID, timeout, destAddr):
	timeLeft = timeout
	
	while 1:
		startedSelect = time.time()
		whatReady = select.select([mySocket], [], [], timeLeft)
		howLongInSelect = (time.time() - startedSelect)
		if whatReady[0] == []: # Timeout
			return (0, 0, 0, ICMP_TYPE_REQ_TIMEOUT, 0) 

		else:
			timeReceived = time.time()
			recPacket, addr = mySocket.recvfrom(1024)
			
			delay = (timeReceived-startedSelect)*1000
			
			header = struct.unpack("hhiblhbbbb", recPacket[:22])
			
			recBytes = ntohs(header[1])
			recTTL = header[3]
			recType = header[8]
			recCode = header[9]
			
			#print(str(recBytes) + " " + str(recTTL) + " " + str(recType) + " " + str(recCode))
			
			return (delay, recBytes, recTTL, recType, recCode)
			
		if timeLeft <= 0:
			return (0, 0, 0, ICMP_TYPE_REQ_TIMEOUT, 0)
#End receiveOnePing

def sendOnePing(sndSocket, destAddr, ID):
	
	# Header is type (8), code (8), checksum (16), id (16), sequence (16)
	myChecksum = 0
	
	# Make a dummy header with a 0 checksum.
	# struct -- Interpret strings as packed binary data
	header = struct.pack("bbHHh", ICMP_TYPE_ECHO_REQ, 0, myChecksum, ID, 1)
	data = struct.pack("d", time.time())
	
	# Calculate the checksum on the data and the dummy header with the checksum equal to 0
	myChecksum = checksum(header + data)

	# Get the right checksum, and put in the header
	if sys.platform == 'darwin':
		myChecksum = htons(myChecksum) & 0xffff
	
	#Convert 16-bit integers from host to network byte order.
	else:
		myChecksum = htons(myChecksum)

	#Add the checksum to the header
	header = struct.pack("bbHHh", ICMP_TYPE_ECHO_REQ, 0, myChecksum, ID, 1)
	packet = header + data

	# AF_INET address must be tuple, not str
	sndSocket.sendto(packet, (destAddr, 1)) 
#End sendOnePing

def doOnePing(destAddr, timeout):
	icmp = getprotobyname("icmp")
	
	#print("doOnePing creating socket...")
	mySocket=socket(AF_INET, SOCK_RAW, icmp)
	#print("done\n")
	
	myID = os.getpid() & 0xFFFF #Return the current process i
	
	#print("Sending one ping...")
	sendOnePing(mySocket, destAddr, myID)
	#print("done\n")
	
	response = receiveOnePing(mySocket, myID, timeout, destAddr)
	
	mySocket.close()
	return response
#End doOnePing
	
def ping(host="www.google.com", timeout=1, numOfPings = 4):
	
	#timeout=1 means: If one second goes by without a reply from the server
	dest = gethostbyname(host)
	
	#Print host w/ IP information if user did typed in host name
	strHost = host if host==dest else host + "[" + dest + "]"
	
	print "\nPinging " + strHost +" with 32 bytes of data:"
		
	#List to hold delays
	listDelays = []
	
	#Count Received packets vs lost packets
	pktRecvd = 0
	pktLost = 0
	
	#Send ping requests to a server separated by approximately one second
	for x in range(0, numOfPings):
		
		#Get response
		response = doOnePing(dest, timeout)
		
		rtt = response[0]
		recBytes = response[1]
		recTTL = response[2]
		recType = response[3]
		recCode = response[4]
		msgReply = ""
		
		if(recType == ICMP_TYPE_ECHO_REPLY):
			listDelays.append(rtt)
			pktRecvd += 1
			msgReply = "Reply from " + dest + ": bytes=" + str(recBytes) + " time=" + '%d' % rtt + "ms" + " TTL=" + str(recTTL)
		
		elif(recType == ICMP_TYPE_REQ_TIMEOUT):
			pktLost += 1
			msgReply = "Request timed Out."
		
		elif(recType == ICMP_TYPE_DEST_UNREACH):
			pktRecvd += 1
			
			#Set correct error msg based on code received
			if(recCode == ICMP_CODE_NET_UNREACH):
				msgReply = "Destination network unreachable."
			elif(recCode == ICMP_CODE_HOST_UNREACH):
				msgReply = "Destination host unreachable."
			elif(recCode == ICMP_CODE_PROTO_UNREACH):
				msgReply = "Destination protocol unreachable."
			elif(recCode == ICMP_CODE_PORT_UNREACH):
				msgReply = "Destination port unreachable."
			elif(recCode == ICMP_CODE_DESTNET_UNKNO):
				msgReply = "Destination network unknown."
			elif(recCode == ICMP_CODE_DESTHOST_UNKNO):
				msgReply = "Destination host unknown."
			else: #General error msg
				msgReply = "Destination unreachable."
		
		time.sleep(1)#1 second delay
		print (msgReply)
		
	print("\nPing statistics for " + dest + ":")
	print("    Packets: Sent = " + str(numOfPings) + ", Received = " + str(pktRecvd) + ", Lost = " + str(pktLost) + \
		" ("+ "{:d}".format((pktLost/numOfPings)*100)  + "% loss),")
		
	if(len(listDelays) > 0):
		print("Approximate round trip times in milli-seconds:")	
		pingmin = '%d' % min(listDelays)
		pingmax =  '%d' % max(listDelays)
		pingavg =  '%d' % (sum(listDelays)/len(listDelays))
		
		print ("    Minimum = " + str(pingmin) + "ms, Maximum = " + str(pingmax) + "ms, Average = " + str(pingavg) + "ms")
	
ping (sys.argv[1] if len(sys.argv) > 1 else "www.google.com")